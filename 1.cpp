#include <memory>

#include <QApplication>
#include <QKeySequence>
#include <QMainWindow>
#include <QtDebug>

#include "qtermwidget.h"

int main (int argc, char** argv)
{
    QApplication app(argc, argv);

    QMainWindow *mainWindow = new QMainWindow();
    QTermWidget *console    = new QTermWidget();
						  
    QFont font = QApplication::font();
    font.setFamily("Hack");
    font.setPointSize(12);
    console->setTerminalFont(font);
    console->setColorScheme("Linux");

    qDebug() << "availableColorSchemes: " << console->availableColorSchemes();

    QObject::connect(console, &QTermWidget::termKeyPressed, mainWindow,
		     [=](const QKeyEvent *key) {
			 if (key->matches(QKeySequence::Copy)) {
			     console->copyClipboard();
			 } else if (key->matches(QKeySequence::Paste)) {
			     console->pasteClipboard();
			 }
		     });

    QObject::connect(console, SIGNAL(finished()), mainWindow, SLOT(close()));

    mainWindow->setCentralWidget(console);
    mainWindow->resize(800, 600);
    mainWindow->show();
    return app.exec();
}
