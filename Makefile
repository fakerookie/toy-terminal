default:
	g++ `pkg-config --cflags --libs Qt5Widgets qtermwidget5` -fPIC -std=c++17 main.cpp -o termi

clean:
	rm -f *.o termi
